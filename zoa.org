* R & D
** TODO [APP] Updater
** TODO [APP] Rapport de crash (?)
** TODO [EXEMPLE] Jeu plus compliqué que si j'étais

* Future

** TODO [APP] Thread safety dans le logger
** TODO [APP] Chargement dans un thread séparé avec barre de loading
** TODO [APP] Template de nouveau projet
** TODO [APP] Changer l'apparence des boutons désactivés
** TODO [APP] Mettre des messages d'erreurs compréhensifs
** TODO [APP] Trouver une font terminus qui supporte le bold
** TODO [APP] Messages d'erreurs sur le chargement des .json
** TODO [APP] Spécifier correctement les tailles et les marges des pages
** TODO [APP] Configurer le DPI souhaité dans le fichier de chargement du projet
- fonction 'configure' dans le build.py
- renommer build.py en builder.py ?
** TODO [ZOA] Charger des KV et des composants spécifiques à zoa
** TODO [ZOA] Déplacer cm dans what pour y accéder dans project.py

** TODO [DEPLOY] Faire un package osx


* 0.1.0

** TODO [APP] Exporter en 72 dpi
** TODO [DEPLOY] Faire un package windows
** DONE [APP] Export sans atlas sur fond transparent
   CLOSED: [2016-08-06 Sat 17:15]
** DONE [APP] Séparer en plusieurs fichiers ( ajouter un module )
   CLOSED: [2016-08-06 Sat 12:35]
** DONE [APP] Mettre correctement des variables d'instances et non de classe
   CLOSED: [2016-08-06 Sat 12:41]
** DONE [APP] Déplacer le dossier d'export au bon endroit
   CLOSED: [2016-08-06 Sat 12:47]
** DONE [APP] Mettre les logs dans le logger
   CLOSED: [2016-08-06 Sat 12:59]
** DONE [APP] Déplacer les fichiers intermédiaires dans un dossier séparés
   CLOSED: [2016-08-06 Sat 12:53]

** DONE [APP] Ajouter un bouton "refresh" actif uniquement quand un projet est chargé
   CLOSED: [2016-08-06 Sat 16:37]
** DONE [APP] Activer le bouton "export" uniquement quand un projet est chargé
   CLOSED: [2016-08-06 Sat 16:37]

** DONE [APP] Améliorer Game pour ranger les items différemment
   CLOSED: [2016-08-06 Sat 16:53]

** DONE [APP] Conserver le ratio des objets à l'affichage
   CLOSED: [2016-08-06 Sat 17:54]

** DONE [APP] Item contenu dans un ScatterLayout pour le rescale
   CLOSED: [2016-08-06 Sat 18:27]
** DONE [APP] Désactiver le multi touch
   CLOSED: [2016-08-08 Mon 08:54]

** DONE [APP] Ajouter un cadre autour de la zone affichable
   CLOSED: [2016-08-08 Mon 09:47]
** DONE [APP] Ajouter un fond et un contour quand il n'y a pas de background
   CLOSED: [2016-08-08 Mon 17:47]
** DONE [APP] Récupérer les exceptions d'éxécution dans un panneau de console
   CLOSED: [2016-08-09 Tue 11:28]
** DONE [APP] Nettoyer la console entre chaque rechargement
   CLOSED: [2016-08-09 Tue 13:46]
** DONE [APP] Comportement de la souris
   CLOSED: [2016-08-09 Tue 14:24]
** DONE [APP] Ajouter des logs dans l'export
   CLOSED: [2016-08-09 Tue 18:19]
** DONE [APP] Export en PDF, un atlas par page
   CLOSED: [2016-08-10 Wed 15:11]
** DONE [APP] Ouvrir le pdf après l'export
   CLOSED: [2016-08-11 Thu 13:36]
** DONE [ZOA] Fichiers séparés et "typedefs"
   CLOSED: [2016-08-09 Tue 14:24]

** DONE [EXEMPLE] Agrémenter l'exemple avec des fichiers json pour les listes
   CLOSED: [2016-08-08 Mon 09:47]
** DONE [EXEMPLE] Formater le texte
   CLOSED: [2016-08-07 Sun 11:50]

** DONE [EXEMPLE] Taper tous les indices
   CLOSED: [2016-08-09 Tue 14:19]
** DONE [EXEMPLE] Ajouter les personnages
   CLOSED: [2016-08-10 Wed 10:57]
** DONE [EXEMPLE] Taper toutes les personnages
   CLOSED: [2016-08-10 Wed 10:57]
** DONE [EXEMPLE] Mettre les cartes au format protège cartes
   CLOSED: [2016-08-11 Thu 13:36]
** DONE [EXEMPLE] Ajouter le miroir pâle sur les personnages
   CLOSED: [2016-08-11 Thu 13:36]
** DONE [BUG] On peut déplacer le dernier élément du GridLayout
   CLOSED: [2016-08-08 Mon 18:06]
Remplacement du ScatterLayout par un RelativeLayout avec une opération de scale
sur le canvas.
** DONE [BUG] Quatre reload font crasher l'appli
   CLOSED: [2016-08-09 Tue 14:19]
