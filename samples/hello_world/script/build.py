# -*- coding: utf-8 -*-

# This module contains all you need to build your game
import zoa.game

# Let's define a simple class, containing only one text property
class Card( zoa.game.Item ):
    text = zoa.game.StringProperty( '' )
    color = zoa.game.StringProperty( '' )

# Zoa will call this function to create your game, this is your entry point and
# where you do all the stuff you need.
def build( game ):

    colors = [ '#12BB23', '#000000', '#AA3444' ]
    # Add an item with the 'hello world!' text
    for color in colors: 
        for i in range( 0, 1 ):
            game.add_item( Card, text = 'Hello World!', color = color )
