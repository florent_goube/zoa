# -*- coding: utf-8 -*-

# This module contains all you need to build your game
import zoa.game

class MoveCard( zoa.game.Item ):
    text = zoa.game.StringProperty( 'Z' )
    color = zoa.game.StringProperty( '' )
    rotation = zoa.game.NumericProperty( 0 ) 

# Zoa will call this function to create your game, this is your entry point and
# where you do all the stuff you need.
def build( game ):

    colors = [ '#FF6EB4', '#EEC900', '#5CACEE', '#FFA54F', '#71C671',
               '#FF3030', '#000000', '#444444', '#B46EFF' ]
    for color in colors:
        game.add_item( MoveCard, color = color, text = 'R', rotation = -28 )
        game.add_item( MoveCard, color = color, text = 'R', rotation = -28 )
        game.add_item( MoveCard, color = color, text = 'R', rotation = 15 )
        game.add_item( MoveCard, color = color, text = 'D', rotation = -30 )
        game.add_item( MoveCard, color = color, text = 'r' )
        game.add_item( MoveCard, color = color, text = 'S')
        for j in range( 0, 4 ):
            game.add_item( MoveCard, color = color )
