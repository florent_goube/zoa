# -*- coding: utf-8 -*-

# on inclus le module zoa
import zoa.game

# création d'une classe qui est un zoa.zoa.game.Item
class Indice( zoa.game.Item ):
    # Propriété de type texte qui sera utilisée par le style
    text = zoa.game.StringProperty( 'Placeholder' )

