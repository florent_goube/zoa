# -*- coding: utf-8 -*-
# Il faut définir l'encodage pour pouvoir utiliser des accents

# on import zoa.game pour pouvoir lire le fichier json, et définir des Property
import zoa.game

# On importe le patron des indices
from .indice import Indice

# On peut aussi définir un patron directement ici
class Character( zoa.game.Item ):
    text = zoa.game.StringProperty( 'Placerholder' )
    source = zoa.game.StringProperty( '' )
    border_color = zoa.game.StringProperty( '#333333' )

# Fonction obligatoire, on ajoute ici les éléments dans le jeu à l'aide de la
# fonction add_item.
def build( game ):

    sample = False
    
    if not sample:
        indices = False
        characters = True

        if indices:
            # pour chaque ligne du fichier indice
            for indice in zoa.game.read_json_file( 'indices' ):

                # add_item( [nom de la classe], [propriétés de la classe] )
                # ici on ajoute un élément de type 'Indice', dont la propriété
                # 'text' est égale successivement à chaque ligne du fichier json
                game.add_item( Indice, text = indice )

        if characters:
            # A chaque deck de personnage on associe une couleur de contour
            # différente
            for deck in zoa.game.read_json_file( 'decks' ):

                # On accède aux propriétés d'un objet json en utilisant
                # l'opérateur [ ]
                # Ici l'objet a les propriétés suivantes :
                # {
                #    'filename': [nom_du_fichier],
                #    'color': [couleur_en_#hexa]
                # }
                filename = deck[ 'filename' ]
                color = deck[ 'color' ]
                for character in zoa.game.read_json_file( filename ):
                    source = ''
                    # si la source est définie dans l'objet character et
                    # qu'elle est non vide
                    if 'source' in character and character[ 'source' ] != '':
                        # on ajoute les parenthèses autour.
                        source = '(%s)' % character[ 'source' ]
                
                    game.add_item(
                        Character, text = character[ 'name' ], source = source,
                        border_color = color )
    else:
        game.add_item(
            Character, text = "Princesse Leia", source = "Starwars",
            border_color = "#333333" )
        game.add_item(
            Character, text = "Dark Vador", source = "Starwars",
            border_color = "#ff00ff" )
        game.add_item(
            Character, text = "Jean Valjean", border_color = "#44ff3f" )
        game.add_item(
            Character, text = "Ballaline", border_color = "#44ff3f" )
        
    
    
