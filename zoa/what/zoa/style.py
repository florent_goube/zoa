import kivy
kivy.require('1.9.1')

import os

from kivy.lang import Builder

def load( name ):
    path = os.path.abspath(
        os.path.join( os.path.dirname( __file__ ), "../../style/" ) )

    filename = os.path.join( path, name )
    return Builder.load_file( filename + ".kv" )
    
    
