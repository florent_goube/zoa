from kivy.graphics import (
    Canvas, Translate, Fbo, ClearColor, ClearBuffers, Scale)

def export_to_png( widget, filename, *args ):
    if widget.parent is not None:
        canvas_parent_index = widget.parent.canvas.indexof( widget.canvas )
        widget.parent.canvas.remove( widget.canvas )

    fbo = Fbo( size = widget.size )

    with fbo:
        ClearColor( 1, 1, 1, 0 )
        ClearBuffers()
        Scale( 1, -1, 1 )
        Translate( -widget.x, -widget.y - widget.height, 0 )

    fbo.add( widget.canvas )
    fbo.draw()
    fbo.texture.save( filename, flipped = False )
    fbo.remove( widget.canvas )

    if widget.parent is not None:
        widget.parent.canvas.insert( canvas_parent_index, widget.canvas )

    return True
