import kivy
import logging

from kivy.clock import Clock
from kivy.uix.label import Label
from kivy.logger import LOG_LEVELS
from functools import partial

COLORS = {
    'TRACE': "ebb6e2",
    'WARNING': "d4c288",
    'INFO': "c2ffd7",
    'DEBUG': "00e2f6",
    'CRITICAL': "faa59b",
    'ERROR': "faa59b" }

class ColoredFormatter( logging.Formatter ):
    def format( self, record ):
        try:
            msg = record.msg.split( ':', 1 )
            if len(msg) == 2:
                record.msg = '&bl;%-8s&br;%s' % ( msg[0], msg[1] )
        except:
            pass
        
        levelname = record.levelname
        if record.levelno == logging.TRACE:
            levelname = 'TRACE'
            record.levelname = levelname

        if levelname in COLORS:
            levelname_color = (
                '&bl;[color=%s][b]%-8s[/b][/color]&br;'
                % ( COLORS[ levelname ], levelname ) )
            record.levelname = levelname_color
            
        return logging.Formatter.format(self, record)

class Logger(logging.Handler):
    
    def __init__( self, label, **kwargs ):
        super( Logger, self ).__init__( **kwargs )
        assert label is not None
        self._label = label
        self._scheduled = False
        self._records = []

    def log( self, dt = None ):
        records = self._records
        self._records = []
        for record in records:
            self._label.text += self.format( record )

    def clear( self ):
        self._label.text = ""

    def emit( self, record ):
        self._records.append( record )
        if not self._scheduled:
            Clock.schedule_once( partial( Logger.log, self ), 0.1 )

        
def setZoaLogger( label ):
    zoa_logger = Logger( label, level = logging.DEBUG )
    zoa_logger.setFormatter( ColoredFormatter(
        '%(levelname)s %(message)s\n' ) )
    ZoaLogger.addHandler( zoa_logger )
    ZoaLogger.clear = partial( Logger.clear, zoa_logger )
    

ZoaLogger = logging.getLogger("zoa.logger")
ZoaLogger.setLevel( level = logging.DEBUG )

__all__ = [ "ZoaLogger" ]

