import kivy.app
kivy.require('1.9.1')

import os

from .mainwindow import MainWindow
from .style import load

from kivy.config import Config
from kivy.core.text import LabelBase


Config.set('input', 'mouse', 'mouse,multitouch_on_demand')

ZOA_FONTS = [
    {
        "name": "terminus",
        "fn_regular": "TerminusTTF-4.39.ttf",
        "fn_bold": "TerminusTTF-Bold-4.39.ttf",
        "fn_italic": "TerminusTTF-Italic-4.39.ttf"
    }
]

kivy.resources.resource_add_path(
    os.path.abspath(
        os.path.dirname( os.path.abspath( __file__ ) ) + "/../../fonts" ) )

for font in ZOA_FONTS:
    LabelBase.register(**font)

class App( kivy.app.App ):
    def build(self):
        load( "zoa" )
        self.root = MainWindow()
        return self.root
