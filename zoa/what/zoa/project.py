import kivy
kivy.require('1.9.1')

import sys
import glob
import imp
import importlib
import os
import shutil
import subprocess

from reportlab.lib.pagesizes import A4
from reportlab.pdfgen.canvas import Canvas
from reportlab.lib.utils import ImageReader

from kivy.atlas import Atlas
from kivy.lang import Builder
from kivy.logger import Logger
from kivy.metrics import cm

import kivy.factory_registers 

from .misc import export_to_png
from .logger import ZoaLogger

from zoa.game import Game

class Project:
    def __init__( self ):
        self._item_list = []
        self._path = None
        self._selfpath = os.path.dirname( os.path.abspath( __file__ ) )
        self._output_directory = None
        self._game = None
    
    def load( self, path ):
        if( self._path is not None ):
            self.unload()
            self._path = None

        self._path = os.path.abspath( path )
        self._output_directory = os.path.join( self._path, "export" )

        return self.load_internal()

    def unload_module( self, name ):
        if name in ('sys', 'marshal', 'imp'):
            raise ValueError(
                "cannot uncache {0} as it will break _importlib".format( name ))
        try:
            del sys.modules[ name ]
        except KeyError:
            pass

    def unload_modules( self, prefix ):
        to_remove = []
        for module in sys.modules:
            if module.startswith( prefix ):
                to_remove.append( module )

        for module in to_remove:
            self.unload_module( module )

    def reload_modules( self, prefix ):
        to_reload = []
        for module in sys.modules:
            if module.startswith( prefix ):
                to_reload.append( sys.modules[ module ] )

        for module in to_reload:
            importlib.reload( module )
   
    def unload( self ):
        self._path = None
        self._game = None
        self._item_list = []

        self.reload_modules( 'zoa.game' )
    

    def load_internal( self ):
        assert self._path is not None
        os.chdir( self._path )
        ZoaLogger.clear()
        ZoaLogger.info(
            "Loading project script in directory %s" % self._path )
        ( file, filename, data ) = imp.find_module( ".", [ "script" ] )
        try:
            imp.load_module( "user.project", file, filename, data )
        finally:
            if file:
                file.close()

        ZoaLogger.info( 'Script found.' )
        kv_list = glob.glob( "style/*.kv" )
        
        try:
            from user.project.build import build

            for kv in kv_list:
                ZoaLogger.info( "KV: Loading " + kv + "..." )
                with open( kv, encoding='utf8' ) as f:
                    Builder.load_string( f.read() )

            self._game = Game()

            ZoaLogger.info( "GAME: Building game..." )
            build( self._game )
        finally:
            self.unload_modules( 'user.project' )
            Builder.unload_file( None )
            importlib.reload( kivy.factory_registers ) 
            importlib.reload( kivy.uix.filechooser )

        ZoaLogger.info( "Game loaded successfully." )
        return self._game._items

    def export( self, pdf ):
        ZoaLogger.info( 'EXPORT: Preparing output directories...' )
        previous_directory = os.getcwd()
        if os.path.exists( self._output_directory ):
            shutil.rmtree( self._output_directory )
        os.makedirs( self._output_directory )
        os.chdir( self._output_directory )

        intermediates = "intermediates"
        os.makedirs( intermediates )

        i = 0
        to_export = []
        ZoaLogger.info( 'EXPORT: Creating intermediate images...' )
        for item in self._game._items:
            filename = intermediates + "/export-" + str( i ).zfill( 3 ) + ".png"
            i = i + 1
            item.size_hint = ( None, None )
            export_to_png( item, filename )
            to_export.append( filename )
        ZoaLogger.info( 'EXPORT: Creating packed pages...' )
        size300dpi = ( 2260, 3270 )
        Atlas.create( "export", to_export, size300dpi, padding = 5 )

        if pdf:
            self.export_to_pdf( size300dpi )

        os.chdir( previous_directory )
        ZoaLogger.info( 'EXPORT: Game exported successfully.' )

    def export_to_pdf( self, size300dpi ):
        size72dpi = ( size300dpi[ 0 ] * ( 72.0 / 300.0 ),
                      size300dpi[ 1 ] * ( 72.0 / 300.0 ) )
        
        size = A4
        canvas = Canvas( 'export.pdf', pagesize = size )
        pos_x = ( size[ 0 ] - size72dpi[ 0 ] ) * 0.5
        pos_y = ( size[ 1 ] - size72dpi[ 1 ] ) * 0.5

        export_files = [ filename for filename in os.listdir('.')
                         if filename.startswith( "export-" ) ]
        export_files.sort( key = lambda x: os.path.getmtime( x ) )

        for filename in export_files:
            image = ImageReader( filename )
            canvas.drawImage(
                image, x = pos_x, y = pos_y, width = size72dpi[0],
                height = size72dpi[ 1 ], mask='auto' )
            canvas.showPage()
        canvas.save()

        if sys.platform.startswith( 'darwin' ):
            subprocess.call( [ 'open', 'export.pdf' ] )
        elif sys.platform.startswith( 'linux' ):
            subprocess.call( [ 'xdg-open', 'export.pdf' ] )
        elif sys.platform.startswith( 'win32' ):
            os.startfile( 'export.pdf' )



    

