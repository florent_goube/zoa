import kivy
kivy.require('1.9.1')

import sys
import traceback

from .project import Project
from .loaddialog import LoadDialog
from .logger import setZoaLogger
from .logger import ZoaLogger

from kivy.graphics import Scale
from kivy.properties import ObjectProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.relativelayout import RelativeLayout
from kivy.uix.popup import Popup

def scale_container( instance, value ):
    scale = value[ 1 ] / instance.initial_size[ 1 ]
    instance._scale.x = scale
    instance._scale.y = scale
    instance.width = instance.initial_size[ 0 ] * scale

class MainWindow( BoxLayout ):
    def __init__( self, **kwargs ):
        super( MainWindow, self ).__init__( **kwargs )
        self._preview = ObjectProperty( None )
        self._project = Project()
        setZoaLogger( self.ids.logger )
        
    def close_popup( self ):
        self._popup.dismiss()
        
    def open( self ):
        content = LoadDialog( load = self.load_project,
                              cancel = self.close_popup )
        self._popup = Popup( title = "Load project",
                             content = content,
                             size_hint = ( 0.8, 0.8 ) )
        self._popup.open()

    def export( self, pdf ):
        try:
            self._project.export( pdf )
        except:
            ZoaLogger.error(
                "Unexpected error: %s\n %s"
                % ( sys.exc_info()[ 0 ], traceback.format_exc() ) )

    def load_project( self, path, filename ):
        self.load_internal( path )

    def load_internal( self, path ):
        self.ids.export_button.disabled = True
        self.ids.export_pdf_button.disabled = True

        self.ids.preview.clear_widgets()
        self.close_popup()

        try:
            items = self._project.load( path )
        except:
            ZoaLogger.error(
                "Unexpected error: %s\n %s"
                % ( sys.exc_info()[ 0 ], traceback.format_exc() ) )
            
            return

        for item in items:
            item.size_hint = ( None, None )
            container = RelativeLayout()
            with container.canvas.before:
                container._scale = Scale( 1 )
            container.size = item.size
            container.initial_size = item.size
            container.size_hint = ( None, 1 )
            container.add_widget( item )
            self.ids.preview.add_widget( container )
            container.bind( size = scale_container )

        self.ids.reload_button.disabled = False
        self.ids.export_button.disabled = False
        self.ids.export_pdf_button.disabled = False

    def reload( self ):
        self.load_internal( self._project._path )
