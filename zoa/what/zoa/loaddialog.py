import kivy
kivy.require('1.9.1')

import os

from kivy.properties import ObjectProperty
from kivy.uix.floatlayout import FloatLayout

class LoadDialog( FloatLayout ):
    load = ObjectProperty( None )
    cancel = ObjectProperty( None )

    def is_dir( self, directory, filename ):
        return os.path.isdir( os.path.join( directory, filename ) )

