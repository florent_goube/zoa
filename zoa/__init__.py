import os
os.environ['KIVY_IMAGE'] = 'sdl2,pil'

import kivy
kivy.require('1.9.1')

import zoa.what.zoa.app as app
import zoa.what.zoa.style as style
        
def main():
    style.load( "loaddialog" )
    app.App().run()
  
