from kivy.uix.image import AsyncImage
from kivy.properties import StringProperty
from kivy.utils import get_color_from_hex

class ImageBox( AsyncImage ):
    img_color = StringProperty( '' )

    def __init__( self, **kwargs ):
        super( ImageBox, self ).__init__( **kwargs )

    def on_img_color( self, instance, value ):
        if self.img_color != '':
            self.color = get_color_from_hex( self.img_color )

    def on_texture( self, instance, value ):
        self.size = self.texture.size
         
