class Game:
    def __init__( self ):
        self._items = []
    
    def add_item( self, klass, **kw ):
        item = klass( ** kw )
        self._items.append( item )
        return item
