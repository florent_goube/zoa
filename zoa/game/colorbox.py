from kivy.graphics import Color, Rectangle, Line
from kivy.uix.widget import Widget
from kivy.properties import ObjectProperty
from kivy.properties import NumericProperty
from kivy.properties import StringProperty
from kivy.uix.floatlayout import FloatLayout
from kivy.utils import get_color_from_hex

class ColorBox( FloatLayout ):
    border_color = StringProperty( "#ffffff" )
    border_size = NumericProperty( 0 )
    background_color = StringProperty( "#ffffff" )
    
    def __init__( self, **kwargs ):
        super( FloatLayout, self ).__init__( **kwargs )

        self._rectangle = None
        self._line = None

        self.rebuildCanvas()
        self.bind( pos = ColorBox.update_content,
                   size = ColorBox.update_content,
                   border_size = ColorBox.rebuildCanvas,
                   border_color = ColorBox.rebuildCanvas,
                   background_color = ColorBox.rebuildCanvas )


    def rebuildCanvas( self, value = None ):
        self.canvas.before.clear()
        with self.canvas.before:
            bg_color = get_color_from_hex( self.background_color )
            Color( *bg_color )
            self._rectangle = Rectangle( size = self.size )
            if self.border_size > 0:
                color = get_color_from_hex( self.border_color )
                Color( *color )
                self._line = Line(
                    rectangle = self.get_rectangle(),
                    width = self.border_size )

    def update_content( self, value ):
        self._rectangle.pos = self.pos
        self._rectangle.size = self.size

        if self.border_size > 0 and self._line is not None:
            self._line.rectangle = self.get_rectangle()
        
    def get_rectangle( self ):
        assert self.border_size > 0
        width = self.width - 2 * self.border_size
        height = self.height - 2 * self.border_size
        return ( self.x + self.border_size, self.y + self.border_size,
                 width, height )



        
        
    
