from kivy.uix.label import Label
from kivy.properties import StringProperty
from kivy.utils import get_color_from_hex

class TextBox( Label ):
    font_color = StringProperty( '#ffffff' )

    def on_font_color( self, instance, value ):
        self.color = get_color_from_hex( self.font_color )
    
