import json

from kivy.uix.widget import Widget
from kivy.properties import ObjectProperty
from kivy.properties import NumericProperty
from kivy.properties import StringProperty

from .colorbox import ColorBox
from .imagebox import ImageBox
from .item import Item
from .game import Game
from .textbox import TextBox

class Spacer( Widget ):
    pass

def dpi():
    return 300

def cm( size ):
    return size * 0.393701 * dpi()

def read_json_file( filename ):
    with open( "data/" + filename + ".json", encoding = 'utf8' ) as data_file:
        data = json.load( data_file )
    return data

